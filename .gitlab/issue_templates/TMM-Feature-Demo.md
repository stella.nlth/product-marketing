<!-- 
Issue Title:
"<Feature Name> (\<Use Case Abbreviation\> Feature Demo)" (eg. "GitLab runners on Azure (CI Feature Demo)") 
don't type the "\" as those are escapes for the greater-than signs. Another example of a "use case abbreviation" is VCC for the use case "Version Control and Collaboration". 
-->

### Summary

Produce a short, focused demo video that highlights the feature in the title. Target a technical to semi-technical audience.

<!-- These next sections down to the end of Publishing are taken from SSoT at https://about.gitlab.com/handbook/marketing/product-marketing/technical-marketing/howto/create-and-publish-demos.html and slightly adapted to match the content type. Please make any necessary changes there. -->

### Video should include:
* [ ] Tell one story across the whole video (have one key take away).
* [ ] What is the feature being shown
* [ ] Why it matters to the audience
* [ ] Show it in action

### Acceptance Criteria
* [ ] Length: <= 3 mins
* [ ] Resolution: 1920 x 1080
* [ ] Casual/Comfortable tone throughout video
* [ ] Introduce yourself and be personable. The video should show you in the beginning and then transition to a demo
* [ ] Feel free to add your own personality. Build a brand.

### Production
* [ ] Develop Demo Script - in partnership w/ PMM, PM, and other stakeholders
   * [ ] Use [this template](https://docs.google.com/document/d/1Cg-8kL71lhoGqiNguh7dDwk4LeofXPKjOsQD0gAWtpo/edit) to encourage collaboration. Link your copy of it in this issue.
* [ ] Configure demo scenario
* [ ] Record Demo

### Post-Production
* [ ] Perform voice overs if necessary
* [ ] When switching subjects in the video, add a section divider (use the "Gradient - Edge" from the FinalCutPro title library)
* [ ] Add cross-dissolves for transitions
* [ ] Add unique style to different parts of video to keep audience engaged

### Publishing
* [ ] Publish demo video to youtube **"GitLab"** channel, playlists: "Learn@GitLab".
   * [ ] Position video in Learn@GitLab playlist within the appropriate use case.
   * [ ] Add CTA's to the end of the new video and fix any other ones so the chain stays intact. See [adding CTA's to Learn videos](https://about.gitlab.com/handbook/marketing/product-marketing/technical-marketing/howto/add-ctas-to-learn-videos.html) for more details.
   * [ ] Create attractive video thumbnails. See [thumbnail guide](https://louisem.com/198803/how-to-youtube-thumbnails) for more details.
* [ ] Publish demo video to Learn@GL page at https://about.gitlab.com/learn/. Details on [asset inventory page](https://about.gitlab.com/handbook/marketing/product-marketing/asset_inventory/)
* [ ] Update the Resources section of the Use Case Resource page, which you can find on the [Usecase GTM page](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/#use-case-gtm-areas-of-interest). Add under "Other Videos" at the bottom of page unless it fits within a Market Requirement.
* [ ] Add the final asset to the [SM Inventory Data spreadsheet](https://docs.google.com/spreadsheets/d/1W5oAlbPV610-ylM7LWv_zc6bEqQK9dI0H9Hrn2f6Jwc/edit#gid=0)
* [ ] Add demo video to [GDrive Demo Folder](https://drive.google.com/drive/u/0/folders/1AWGh_v8Gn26RYhPYmc4jWor-RgqvngRZ) in the appropriate folder.
* [ ] Announce new video (with link) to Slack channels: #marketing

<!-- Don't remove anything below this line -->
/label ~"Strategic Marketing" ~"mktg-status::plan" ~"sm_request" ~"sm_req::backlog" 
/label ~"usecase-gtm" ~"tech-marketing" ~"tech-marketing::demo"
/weight 1

<!-- Add from below this line as necessary: 
/label ~"\<label for the use case\>"
/milestone %\<milestone\> 
/epic &\<XXXXXX\> 
/assign @\<assignee\>
   + /label ~"sm_req::assigned"
/due \<due date\>
--\>
