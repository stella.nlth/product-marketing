**Template Overview:** Use this template to request industry analyst contact recommendations from [GitLab analyst relations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/) in support of customer/prospect engagement.

**Requestor:** 

**Research Topic/Use Case:**

**Company Name of Customer/Prospect:**

**Recommendation Requested By:**

**Requested Analyst Firm(s):**

**Known Competitors:**

**Additional information that would help the analyst relations team identify the most appropriate analyst(s) for this company to speak with:**

*



**NOTE:** Access to analyst(s) is dependent on the company's contract status with the analyst firm in question. 


/label ~"Analyst Relations" ~"AR-Research" ~"Strategic Marketing" ~"sm_req::new_request" ~"mrnci" ~"mktg-status::plan" ~"sm_request" ~"pmM::Internal"
/assign @rragozzine
/confidential
