**Strategic Marketing QBR Action Item**

* **Topic**:<title/topic>
* **Point of contact/ person requesting:**: 

* **Details**: 
<Details please>


[QBR Action Items Doc](https://docs.google.com/document/d/1V5wILrc7NdPIHCKAPnM-4SccLwbbn0718xetd_A2nK0/edit?usp=sharing)


/epic https://gitlab.com/groups/gitlab-com/marketing/strategic-marketing/-/epics/275
/label ~"Strategic Marketing" ~"FY22-Q1" ~smqbr ~"sm_request" ~"sm_req::new_request" ~"mktg-status::plan"
/weight 1
