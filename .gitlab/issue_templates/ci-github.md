**GitHub Competitive Intel Issue**  

## sub head

*  Event / Meeting support
*  Content creation / review
*  Demo or workshop


/confidential
/label ~"Competitive Intelligence" ~"Competitor - GitHub" ~"Strategic Marketing" ~"sm_req::backlog" ~"mktg-status::plan"
/milestone %"SM - Backlog" 

/epic https://gitlab.com/groups/gitlab-com/marketing/-/epics/573
