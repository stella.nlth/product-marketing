# **Title: Marketing Insights Support Request**  
<!--

<!-- Note: Please use this template to request support from the Marketing Insights team for research, data review, customer calls, topic submission, thought leadership content, etc. -->

## Type of Help Requested
<!-- Select what is applicable from the list below and delete the rest -->

- [ ]  Research Submission (_technology/market/trend/topic/industry/vertical/use case, user or persona, etc._)
- [ ]  Data Review & Interpretation
- [ ]  Customer/Prospect Call
- [ ]  Event speaking - STOP - Please fill out this [PMM Speaker Request form](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=pmm-speaker-request) and request @traci 
- [ ]  Thought leadership content creation / review
- [ ]  Other 


**Requestor:** 

**Additional team members involved:**

**Research topic:**

**Company/Organization name of customer/prospect:**

**Target completion date:**

**Next steps:**

**Date needed:**



## Request Info
<!-- Please add as much detail as you can -->



+ Related issue(s): 
+ Related documents/meeting notes/reports, etc.: 
+ Related links: 


##  Request Details
<!-- Please provide additional details for this request: include hypothesis, expectations, positioning, goals, specific deliverables needed, etc. --> 
[What is your business objective and underlying rationale for this research?]
[Define the questions that you want to answer.]


**End of template**


/label ~"Market Insights"  ~"Market Research" ~"mktg-status::plan" ~"Market Strat and Insights"
/assign @traci
/weight 1
