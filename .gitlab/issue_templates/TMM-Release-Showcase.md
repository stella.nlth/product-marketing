<!-- 
Issue Title: "<Version> Release Showcase"
-->

<!--
#### Instructions
1. Copy the [template](https://docs.google.com/presentation/d/1-22nS_N6EE27sgjeITtNf_4P7qvXin7h4stYXqOwhmw/edit?usp=sharing) once into the [Release Showcase folder](https://drive.google.com/drive/u/0/folders/11rJoj12OXeq7XCcfyg1nr8rEfLWvHSXB)
2. Edit copy for current release #
3. Put a link to the new file for this release under the links section so others can find it easily
4. Add the pointer to the release post review app (usually can be found in the #release-post channel)
-->

##  Request Details

TMM team to present “what’s new” showcases of new capabilities in latest release (see title for version).

A showcase consists of short presentation of what is it, customer value, resources, and a short demo OR screenshot walk through. This should be reasonably technical, as if giving to SAs and TAMs, not high level. See [slides](https://gitlab.com/gitlab-com/sales-team/cs-skills-exchange/-/issues/81) and [video](https://youtu.be/yWzQxFGPgT8) of a previous release showcase for examples.

#### Links
- [Release Post Review App]() <!-- add latest release post review app here -->
- [Working Presentation for this release]() <!-- copy template, customize for new release, add link here -->

#### Details
- We will record ourselves doing preso and demo, and put together as a learning path on Learn@GitLab to share.
- We will NOT host a CS Skills Exchange meeting for this content unless specifically asked to
- Presentations to be sent out on <release date + about 2 weeks>
- 5 minutes max per subject (would like the total runtime for each release showcase to be no longer than 60 minutes)

#### Signups
Please add in the table what feature(s) you plan to do so that others don't take work on the same thing.

| TMM | feature(s) |
| ------ | ------ |
| Cesar | ? |
| Fern | ? |
| Itzik | ? |
| Tye | ? |
| William | ? |

#### Considerations
- Each video should be stored in a Google Drive
- Make sure your audio is not too soft, not too loud

#### Publish steps

##### Individual
- [ ] Upload individual segments to YouTube under Learn Playlist
   - [ ] A thumbnail should be applied to each
   - [ ] A good description with links back to Learn should be applied to each
- [ ] Add to Learn@GL Playlist

##### Publish DRI
- [ ] Assign Publish DRI for this showcase
- [ ] Create Learn@GL learning path for the release showcase
- [ ] Announce availability and link folks to the learning path on learn@GL
- [ ] Add to Field Communication issue for the month

## Timelines
- [ ] Strive to have the Release Showcase published on Learn@GL no later than 2 weeks past the release.


<!-- Don't remove anything below this line -->
/label ~"Strategic Marketing" ~"mktg-status::plan" ~"sm_request" ~"sm_req::assigned" 
/label ~"usecase-gtm" ~"tech-marketing" ~"tech-marketing::demo"
/weight 1
/assign @csaavedra1 @fjdiaz @iganbaruch @davistye @warias 
/due \<due date\>.  <!-- make this the same date as the meeting -->

<!-- Add from below this line as necessary: 
/label ~"\<label for the use case\>"
/milestone %\<milestone\> 
/epic &\<XXXXXX\> 
/assign @\<assignee\>
   + /label ~"sm_req::assigned"
--\>



