**Start of the template.**

**Analyst Firm:**

**Title:**

**Questionnaire Due date:** 

**Primary Analyst(s):** 

**Primary Contact:** 

**Document List**
*  Initial email
*   Draft RFI
*   Final RFI

**DRI's**
*  Analyst Relations @rragozzine
*  PMM
*  PM
*  TMM 
*  Customer References @jlparker @fionaokeeffe
*  Other interested @cfletcher1 @nwoods1 @cweaver1 @jgragnola

**To do**
* [ ] (Forrester only Kickoff call for ALL VENDORS scheduled for: 
* [ ] (Forrester only) Draft version of the evaluation criteria expected by:
* [ ] (Forrester only) Feedback on the draft version of the evaluation criteria due by:
* [ ] Create report epic to manage communications issues, including [announcement request issue](https://about.gitlab.com/handbook/marketing/corporate-marketing/#pr-requests-for-announcements) (**Note:** Please make the due date on the announcement issue the projected date that preliminary results are expected), blog, organic social, commentary page, etc.
* [ ] Final survey expected date: 
* [ ] Completed survey due date: 
* [ ] Recorded demo due date (if applicable): 
* [ ] Executive Briefing/Demo to be scheduled by:
* [ ] Customer references due date: 
* [ ] Draft review of GitLab due date (issue):
* [ ] Determine whether to purchase reprint rights (reflect in epic)
* [ ] Write blog post or NA (optional; see epic)
* [ ] Update analyst research page page
* [ ] Create GitLab commentary page or NA (see epic)

**Related issues and links**
*  Report epic
*  Drive folder
*  Presentation issue
*  Demo issue
*  Folder for previous Wave/MQ, if applicable


/label ~"Analyst Relations" ~"Strategic Marketing" ~"AR\-WaveMQ" ~"mktg-status::wip" ~"mrnci" ~"sm_req::assigned" ~"sm_request" ~"pmM::External" ~"ar-p::1"
/assign @rragozzine
/confidential
