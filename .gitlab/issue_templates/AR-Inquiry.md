<!--

This template is for the Industry Analyst Relations team to use to schedule an inquiry. Please do NOT use this template to REQUEST an inquiry.

To request the IAR team to set up an inquiry, please use the AR-ResearchRequest template above and fill that out.

-->

**Analyst firm:**

**Analyst(s):**

**Topic:**

**Use Case(s)**

**GitLab attendees:** 

**Inquiry date:**

**Supporting documentation/meeting notes:**
- Agenda/notes doc
- Research request issue

/label ~"Analyst Relations" ~"Strategic Marketing" ~"AR\-Inquiry" ~"mktg-status::scheduled" ~"mrnci" ~"sm_req::assigned" ~"sm_request" ~"pmM::Internal"
/assign @rragozzine
/confidential
