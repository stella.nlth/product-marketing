# Virtual Event Details
* **Owner:** Name @username
* **Type:** Virtual Event
* **Date:** 
* **Website:** 
* **Budgeted Cost:** `fill in`

#### What will this virtual event contain/require? - if "MAYBE" please indicate estimated date of decision.
⚠️ **This section must be filled out by the owner before the MPM creates the epic and related issues. [See handbook.](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/#events-process-for-marketing-programs--marketing-ops)**
* Follow up email via Marketo - YES/NO
  *  Will the copy rely on content from the virtualevent? i.e. copy due 24 hours post-event (this will push out the send 2 business days). - YES/NO
* Landing page creation (requires promotion plan) - YES/NO
* Invitation and reminder emails (requires promotion plan) - YES/NO

/label ~"mktg-status::plan"