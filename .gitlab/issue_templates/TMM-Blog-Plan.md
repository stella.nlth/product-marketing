<!-- 
Issue Title:
"(\<Use Case Abbreviation\> Tech Blog Plan)" (eg. "CI Tech Blog Plan") (don't type the "\" as those are escapes for the greater-than signs). Another example of a "use case abbreviation" is VCC for the use case "Version Control and Collaboration".  
-->

Technical Marketing is responsible to create at least 1 technical blog per use case per quarter. This issue is to create the blog plan (blog subjects) that will be used for these.

Define 6-7 technical blog ideas that fit within your use case space (we really are on the hook to do 4, but need buffer in-case ideas get stale, or done by someone else, etc). We can iterate on these.

* Pick subject matter that will be sharing/teaching some knowledge that fits into this use case
* Pick subjects that either most don't know or that shares a way to do something no one thought of or figured out so far (with our product)
   * example - matrixed builds using DAG
* You should validate that what you put on this list as a possibility is not already well covered (do a google search for published blogs) and if it is either figure out how to do a better version, or ditch that idea.
* These technical blogs are expected to be sharing code, projects, screenshots, etc, not just ideas. If the blog subject is a good one, there is most likely some research/learning/development that you'll need to do to present the subject well. 
* It's ok to do a series
* This list should **exclude** smaller quick blogs like pointing to new demo videos.

Other acceptable tech blogs ideas:
* Newly released technical features and their value
   * Can range from small - large features
* Technical tutorials
   * Setting up a particular component within GitLab
   * Integrations with 3rd party tools
   * Integrations with technology partners
* Technical details on how GitLab benefits particular use-cases
   * Example: GitLab for infosec

## Blog Plan
(add your blog plan here)
1.  
1.  
1.  
1.  
1.  
1.  
1.  


<!-- Don't remove anything below this line -->
/label ~"Strategic Marketing" ~"mktg-status::plan" ~"sm_request" ~"sm_req::backlog" 
/label ~"usecase-gtm" ~"tech-marketing" ~"tech-marketing::blog"
/weight 1

<!-- Add from below this line as necessary: 
/label ~"usecase-ci"  <!-- (label for the use case) --\>
/milestone %\<milestone\> 
/epic &\<XXXXXX\> 
/assign @\<assignee\> <!-- if you are assigning it on creation --\>
   + /label ~"sm_req::assigned" <!-- if assigning --\>
/due \<due date\> <!-- if assigning --\>
-->

