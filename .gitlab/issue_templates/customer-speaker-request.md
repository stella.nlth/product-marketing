**Title: Strategic Marketing Support Request-Customer Speaker**  

<Note: Please use this template to request support from the Strategic Marketing team for sourcing customer speakers for webinars, events, meetings, content creation/review, research, etc.>
[SM Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?&label_name[]=sm_request)

## Type of help requested  <Select the type of help you need from the list below and delete the others>

*  Customer Speaker - Event

Please note the reference team prioritizes customer speaking requests from the demand generation teams (FMM/Campaigns/Alliances/Partners and Corporate). We support an average of 12 customer speaking requests per quarter. 

## Request Info:

All information **needs** to be completed before we can begin the search for a speaker. The requestor is responsible for supplying this information.

+ Event Type:
+ Event Title:
+ Event Date:
+ Event Campaign Tag (Salesforce) : `please link here`
+ Event/Meeting Location:
+ Desired Speach Topic/Title:
+ Desired Speach Duration:
+ Audience for the talk:  (GitLab Prospects, GitLab Users, GitLab Employees)
+ Desired outcome of the speach:
+ Deadline for sourcing Speaker (need min 2 weeks post request raised): 
+ Related Issue: <link>
+ Point of contact/ person making the request:
+ Is there a person you would like?: 
+ Is the event live or recorded? 
+ Is there a paid media promotional plan in place for this event?


## Please Note

- The timing requirements per event type that the Reference Manager needs are outlined in the [handbook](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#requesting-a-reference-for-an-event)

Check the following (all items must be checked before you submit the request)

- [ ] Affirm you have read through the [Customer Reference Event Requirements](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-events.html#customer-speaker-management), please note requests that do not meet timing requirements may not be able to be supported, if Customer Reference Team is supporting past the minimum requirement, we may require additional approvals.
- [ ] Once you raise this issue, please slack your [local Reference Manager](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) with a link to the issue.
- [ ] If travel is required for a customer speaker, it is covered by field marketing.
- [ ] The CRM will ask a maximum of 2 customers for a speaking opportunity. 

### Post Reference - Cleanup

- [ ] CR to confirm [this Salesforce Event Report](https://gitlab.my.salesforce.com/00O4M000004aKoh) is updated and tagged with the appropriate customer reference speaker that the CR team provided for the event.
- [ ] Track Reference Activity in Reference Edge



/label ~"sm_request" ~"Strategic Marketing" ~"sm_req::new_request" ~"mktg-status::plan"
/weight 1
/label ~mrnci 
/label ~"Customer Speaking Requests" 
