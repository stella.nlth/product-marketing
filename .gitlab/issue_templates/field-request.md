
## Field - Strategic marketing request

*Please describe your idea, request, suggestion so we can discuss and prioritize.




* **Category of what you want:** 
   * i.e.: *(messaging, enablement, new content, restructure content, research, demo, other)*

* **Problem to solve:**
  
* **Benefit:**

* **When:**

* **Additional Details:**



/label ~"sm_request" ~"Strategic Marketing" ~"sm_req::new_request" ~"mktg-status::plan"
/label ~"field-request"
