MPM to create this issue and name using the following format: `Persona / Positioning Matrix - Name of Campaign` (then delete this line)

## Purpose

Provide final draft for persona / positioning / messaging for `[name of campaign]` in the linked googledoc below. This will then be used by all other teams as a guide and reference for the campaign - it is specifically a dependency for Content Marketing to create a Content Journey Map by buyer stage.

*Examples from previous campaigns are listed to the right of the spaces to be filled in, as examples.*

### [Link to persona/positioning/messaging matrix]() 

`MPM to add direct link above to appropriate tab in campaign execution timeline, and then remove this note`

*Note to MPM: [this is the overall process template which will be clone for the campaign](https://docs.google.com/spreadsheets/d/1VTrWNX9qfY99b2TnrX93P39aXiRoNnChB6tduTvmysA/edit#gid=189445634)

/label ~"MPM Integrated" ~"Marketing Programs"

<!-- Please leave the label below on this issue -->
/label ~"Strategic Marketing" ~"mktg-status::plan" 
