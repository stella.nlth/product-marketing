# **Title: Portfolio Marketing Speaker Request**  

<!-- Note: Use this template to request pmm or orther person from Portfolio marketing to speak at an event -->

# Originating event

- [ ] link to related issue with more details: 
- [ ] Requested topic:

## Details

- Event Title:
- Event link (showing agenda, etc)
- Desired abstract: 
- Event Date:
- Event Time: 
- Event Location:
- Point of contact/ person making the request:
- Person requested: 
- Link to Landing page copy: 
- Target Audience/Persona: 
- Other related Issue(s): 


## Type of event/participation requested
<!-- Select the type of help you need from the list below and delete the others -->

- [ ] Speaker, presenting existing content (from existing content that has been presented before. Please link to the content that you want presented again. As a non-exhustive example, you may find some ideas in the [PMM talk menu/inventory](https://docs.google.com/spreadsheets/d/1sQdR4mMNKWep0v02HQacOCEfb0BP19jFygTxnvUcau8/edit#gid=0). Chosing existing content is faster and more efficient.)
- [ ] Speaker, Develop **NEW** content for an event (note will take more lead time and prioritization discussion)
- [ ] Field live Q&A for a pre-existing video being replayed
- [ ] Moderator (panel discussion, or non-pmm content presenter) 
- [ ] Attend the event and network
- [ ] Video Press interview
- [ ] Other (describe the format and the specifcs of the ask) 

### Format Details 

Such as: How long, and what format? E.g. "1 hour long, 45 min presnetation, 15 min Q&A", "30 min round table discussion. You moderate with 2 guest panalists" , conversational, etc.   

## Post-event info

### Asset/replay
- **Final Asset(s):** 

### Leads

- **Live Views:**
- **On-demand Downloads:**
- **Marketing qualified Leads generated:**
- **Sales accepted leads:**
- **Feedback for next time:**

### Cross-promotion

- [ ] asset link entered into PathFactor or shared with digital marketing team
- [ ] associated GTM team notified in slack
- [ ] asset loaded into HighSpot if applicable

cc @jfullam @davistye

/label ~"sm_request" ~"Strategic Marketing" ~"sm_req::new_request" ~"pmm" ~"pmm::event"

