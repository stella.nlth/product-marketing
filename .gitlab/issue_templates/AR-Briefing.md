**Analyst firm:**

**Analyst(s):**

**Topic:**

**Other background:**

**Briefing date(s):**

**GitLab presenters/attendees:**
- Presentation: 
- Demo (as needed): 
- Others interested:  

**Supporting documentation/Meeting Notes:**
- Agenda/Notes Doc

**Link to presentation:**

**Follow-up inquiry scheduled (as needed):**


/label ~"Analyst Relations" ~"AR\-Briefing" ~"sm_req::assigned" ~"Strategic Marketing" ~"mrnci" ~"mktg-status::plan" ~"sm_request" ~"pmM::Other"
/assign @rragozzine
/confidential 
