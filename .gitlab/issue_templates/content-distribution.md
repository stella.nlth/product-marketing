Much of the information requested below will be used in multiple places as you distribute the content. Write it once here and simply copy/paste into the various templates without having to re-think about it.

**Content name:**

**Content type:**
- [ ] whitepaper
- [ ] blog
- [ ] video
- [ ] other: 

**Content location (URL):**

**Content creator**

# Metadata needed for distribution process

**One sentence description** (think SEO/search):

**Solution it supports**
* **C-level**
   - [ ] Digital transformation
   - [ ] App modernization
   - [ ] Cloud transformation
   - [ ] Security and compliance at the C-level
* **Manager-level**
   - [ ] Software delivery automation
   - [ ] Security
   - [ ] Compliance
   - [ ] Platform
* **Capabilities / use cases**
   - [ ] SCM
   - [ ] CI
   - [ ] CD
   - [ ] GitOps
   - [ ] DevSecops
   - [ ] Supply chain security
   - [ ] Compliance

**Intended audience:**
* C-level, VP/Dir/Mgr, individual contributor: 
* Region: 
* Ent, Commercial (SMB/MM):

**Intended persona**
* buyer:
* user:

**Stage in buyer's journey:**
- [ ] awareness/ top of funnel
- [ ] consideration/ mid-funnel
- [ ] preference/purchase/ bottom of funnel


# Distribution considerations

## Web  
- [ ] solution page - PMM does it themselves then requests approval from web team. repo is now [here]( )
- [ ] web banner - request from web team [here]( )
- [ ] 'features' page - PMM does it themselves then requests approval from web team. repo is now [here]( )
- [ ] other page: 

## PathFactory - link to instructions

## HighSpot - link to instructions
- [ ] load content itself (pdf, not a web link) to HS content, add all labels, expiration date, etc.
- [ ] link content from a related solution under solution selling as Things to show. This section should be kept to roughly 6 pieces. If there's more, consider removing other less powerful content. 

## YouTube
* unfiltered
* other


