**Firm:** 

**Item(s):**

**Description:** 

**Additional context:** 

**AR lead:** @rragozzine 

**Additional team members involved:** @Daryayar @lclymer @rcallam 

**Target completion date:** 

**Next steps:**

- [ ] Confirm receipt of proposal (upload; link below) or N/A
- [ ] Engage procurement for negotation assist or N/A
- [ ] Confirm receipt of purchase agreement (upload; link below)
- [ ] Create Coupa purchase req (link below)
   - [ ] Send requested modifications to vendor for review or N/A
   - [ ] Upload modified agreement to Coupa or N/A
- [ ] Secure internal signature
- [ ] Secure firm signature (upload signed agreement; link below)
- [ ] Confirm PO finalized (link below)
- [ ] Label complete; close issue

**Links/Resources:**
- [Drive folder]
- [Meeting notes doc]
- [Proposal]
- [Purchase agreement]
- [Coupa req]
- [Signed agreement]
- [Coupa PO]

/label ~"Analyst Relations" ~"AR-Internal" ~"sm_req::assigned" ~"Strategic Marketing" ~"mrnci" ~"mktg-status::plan" ~"sm_request" ~"pmM::Internal"
/assign @rragozzine
/confidential
