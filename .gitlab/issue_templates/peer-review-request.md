# **Title: Peer Review Support Request**  

<!-- Note: Please use this template to request support from Peer Reviews for events, meetings, content creation/review, research, etc. -->

<!-- After submitting this request you'll be able to --> Track your request on the [SM Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?&label_name[]=sm_request) and the [Customer Reference Program Board]

## Type of Help Requested
<!-- Select the type of help you need from the list below and delete the others -->

- [ ]  Peer Review Quote Request
- [ ]  Peer Review Source Evaluation
- [ ]  Peer Review Metrics/Analysis
- [ ]  Peer Review Quote Approvals
- [ ]  Other: 


## Request Info
<!-- Please add what you can -->

+ Intended Use: eg. Sales Deck, Event Presentation
+ Request Due: (please provide 5-7 business days for research requests)
+ Requester: (use @gitlab id)
+ Person requested: @jlparker 
+ Related Issue(s): 


##  Request Details
<!-- Please provide additional details of activity: include expectations, positioning, goals, and specific skills needed --> 

+ Description of what you need:

<!-- Please leave the labels below on this issue -->
/label ~"Customer Reference Program" ~"Peer Review Request" ~"mrnci" ~"sm_request" ~"Strategic Marketing" ~"sm_req::new_request" ~"mktg-status::plan"