# **Portfolio Marketing Support Request**  

<!-- Note: Please use this template to request support from the Portfolio Marketing (was called Strategic Marketing) team for events, meetings, content creation/review, research, etc. -->
<!-- After submitting this request you'll be able to --> Track your request on the [SM Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?&label_name[]=sm_request)

## Type of Help Requested
<!-- Select the type of help you need from the list below and delete the others, note that multiple can be selected -->

### Events / Meetings
- [ ]  Speaking - Please fill out this [PMM/TMM Speaker Request form](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=pmm-speaker-request)
- [ ]  Support (booth, demos, etc)

### Content Delivery
- [ ]  Content creation / review
- [ ]  Demo or workshop

### Other
- [ ]  Research
- [ ]  Competitive analysis
- [ ]  Customer Reference/Case Study
- [ ]  Partner Marketing

## Request Info
<!-- Please add what you can -->
* Person making the request: 
* Person requested (if knwon):
* Originating/related issue:
* Related CMO OKR: 
* When must a resource be identified:


## Post-Request Information
<!-- Please fill out when the Event has been Completed --> 

### Completed Assets

- **Final Asset(s):** 

- **Live Views:**
- **On-demand Downloads:**
- **Marketing qualified Leads generated:**
- **Sales accepted leads:**
- **Feedback for next time:**

### Cross-promotion

- [ ] asset link entered into PathFactor or shared with digital marketing team
- [ ] associated GTM team notified in slack
- [ ] asset loaded into HighSpot if applicable

## SM section
#### [Refinement questions](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/sm-project-management/#workflow---refine-sprint-retrospective)
When reviewing this issue/ request consider the following:

- [ ] **DRI**: Has a DRI been identified and do they accept responsibility? Be sure to assign them once accepted.
- [ ] **Clear**: Are the expectations and work required unambiguous and clearly defined?
- [ ] **Completable**: Is there a clearly-stated definition of done?
- [ ] **Unconstrained**: Is the work free of blockers and dependencies? *If not, address blockers first or leave in Product Backlog until blockers are removed.*
- [ ] **Achievable**: Is the issue something that can be completed within a 2-week timebox? *If not, decompose it.*

/label ~"sm_request" ~"Strategic Marketing" ~"sm_req::new_request" ~"mktg-status::plan" ~"refine::unrefined"
/weight 1
