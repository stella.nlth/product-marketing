**Analyst Firm:**

**Advisory Topic:**

**Additional Background:**

**Primary Analyst(s):** 

**Primary Analyst Firm Contact:** 

**Planned Duration:**

**Scheduled Date:**

**Document List**
*  [Meeting notes]
*  [document name]
*  [document name]

**Team Members:**
*  Analyst Relations @rragozzine
*  Product Marketing
*  Product Management
*  Technical Marketing
*  Competitive Intelligence
*  Other interested @lclymer

**To do**
* [ ] Confirm budget / available credits
* [ ] Open PO, if necessary
   * Coupa link here
* [ ] Schedule scoping call
* [ ] Schedule advisory session / day
* [ ] Provide background / review materials to analyst
* [ ] Provide backgrounder to GitLab participants (analyst research, prior meeting notes/insights)
* [ ] Prepare presentation materials for real-time feedback or N/A
* [ ] Prepare demo for real-time feedback or N/A


**Related issues and links**
*  [Demo issue]  
*  [Presentation issue]
*  [Recent analyst research]

/label ~"Analyst Relations" ~"Strategic Marketing" ~"AR-Strategy" ~"mktg-status::wip" ~"mrnci" ~"sm_request" ~"pmM::Internal" ~"sm_req::assigned"
/assign @rragozzine
/confidential
