**Quarterly Earnings Statement Customer Reference Process:**

Earnings Statement Reference support should begin at the start of each new quarter, with the following immediate objectives:
To engage with an identified list candidate customers from three categories First Order, Growth, and Microsoft Wins. The earnings statement customer reference activity will work towards the following targets in terms of reference customers

**First Order**
These are new customers - largest 25 in terms of deal size
Goal: CRM team to source 3 named highly recognized customers and logos; customer to provide legal permission to mention their business by name and use their logo in earnings statement.

**Microsoft wins**
These are new customers where the other product under consideration was MS - largest 25 in terms of deal size
Goal: CRM team to source 3 named highly recognized customers and logos; customer to provide legal permission to mention their business by name and use their logo in earnings statement

**Growth**
These are existing customers who increased their spend/number of licenses - largest 25 in terms of deal size
Goals:
Customer to provide legal permission to mention their business by name and use their logo in earnings statement (if not already received).
In collaboration with the customer, create a named Enterprise customer story that contains a 2-3 sentence quote/story on impact we are having. For the 2 - 3 sentence quote/story we might want to think about additional framing i.e. the reason the customer decided to purchase/business need we are addressing.
If the customer is unwilling to provide named enterprise story, in collaboration with Customer Success create Anonymous customer story (enterprise). that contains a 2-3 sentence quote/story on impact we are having. For the 2 - 3 sentence quote/story we might want to think about additional framing i.e. the reason the customer decided to purchase/business need we are addressing. CRM team to source 3 enterprise named highly recognized customer stories, anonymous customer story write-up for all remaining highly recognized customers.

Note: for Growth accounts who do not consent to be a reference, we can create anonymized references.  CR to work with SAL and TAM should this occur as standard process.


### First week of new Quarter:
- [ ] CR to create new issue using the CR-Earnings-Reference-Support.md template in Product Marketing
- [ ] Link the issue to the [Quarterly Earnings Reference Support Epic](https://gitlab.com/groups/gitlab-com/marketing/strategic-marketing/-/epics/379)
- [ ] Assign to @jlparker @fionaokeeffe @lclymer
- [ ] Identify Target accounts and load to [this spreadsheet](https://docs.google.com/spreadsheets/d/1X7qCHsaOAtH-a-A-_R6I0eWQh3cwxlkHah8Pr4OT_bc/edit#gid=0) (DRI:Sales Finance. CRM to request report 1 week prior to new quarter)
- [ ] Confirm with @cmestel that this report is accurate for the earnings reference work
- [ ] Identify Sales/Customer Success and Execs contacts required to support this effort - `mention them here` (DRI:CRM)
- [ ] Update the [Reference Support Deck](https://docs.google.com/presentation/d/1Z72PAmwJJgir0BAjyiOLFGH5qHnsodeQdfqTjD5P-84/edit?usp=sharing) with updated requests (new target accounts tab, approved email request copy and authorization form, specifics of the request i.e. logo, testimonial statement, financial decks)
- [ ] Confirm with legal - `mention them here` that we have the appropriate [email draft copy](https://drive.google.com/drive/folders/12I2QautB2w4aPENZMv1Y2lZd3uuind6v?usp=sharing) and [authorization form](https://drive.google.com/file/d/1cim8ZlkIw57hHxYPit97xY-sCeKqMMLJ/view?usp=sharing) 
- [ ] Pin these assets to the #earnings_customer_references channel for easy access
- [ ] Add all GitLab internal support to #earnings_customer_references channel - CR to delegate individual support for each account targeted

### Ongoing: 
- [ ] CR team to identify lead CR supporting individual accounts and following up with Account Owners and Execs re: request status and dependencies/blockers
- [ ] CR to maintain data fidelity in [this spreadsheet](https://docs.google.com/spreadsheets/d/1X7qCHsaOAtH-a-A-_R6I0eWQh3cwxlkHah8Pr4OT_bc/edit#gid=0)
- [ ] As approvals come in, CR to add to appropriate [approval folder](https://drive.google.com/drive/folders/12OwV53YvmEVnnbNdoqOmQrGUE1_brK4d?usp=sharing) in G-Drive
- [ ] For any Growth accounts who do not give consent, CR work with the appropriate SAL/Account Team to create anonymized statement.

### Post-Statement:

- [ ] CR to track Reference activity in ReferenceEdge
- [ ] CR to run report of reference activity and load into [Quarterly Earnings Reference Activity Report - link TBD]()
- [ ] Review logos where we now have general web usage and add to the quarterly brand review issue of our [approved logo deck](https://docs.google.com/presentation/d/1UKVfxDm6KNpWCjoAcWp0AfX1yaLT10FK-zbbFZLAwZE/edit#slide=id.gc300321b28_0_273)
- [ ] Add new approved logos to the [external approved logo sheet](https://docs.google.com/spreadsheets/d/1wnczZQ7a8rIXI_OCNPuqJv5VeC5gGs-lwAmlgyxUlvU/edit?ouid=118300580366217548304&usp=sheets_home&ths=true)







###### Important Links

- [Quarterly Earnings Reference Support Folder - G-Drive](https://drive.google.com/drive/folders/12OwV53YvmEVnnbNdoqOmQrGUE1_brK4d?usp=sharing)
- [email draft copy](https://drive.google.com/drive/folders/12I2QautB2w4aPENZMv1Y2lZd3uuind6v?usp=sharing)
- [authorization form](https://drive.google.com/file/d/1cim8ZlkIw57hHxYPit97xY-sCeKqMMLJ/view?usp=sharing)
- [Reference Support Deck](https://docs.google.com/presentation/d/1Z72PAmwJJgir0BAjyiOLFGH5qHnsodeQdfqTjD5P-84/edit?usp=sharing)
- [Target account spreadsheet](https://docs.google.com/spreadsheets/d/1X7qCHsaOAtH-a-A-_R6I0eWQh3cwxlkHah8Pr4OT_bc/edit#gid=0)


/label ~"Customer Reference Program" ~"Quarterly Earnings CR Support"
