Title: PubSec Partner Content Marketing Request
<Note: Please use this template to request support from the PubSec PMM for partner content creation/review.>
SM Triage Board

#### Type of help requested  

 [ ] Content creation / review

 [ ] Customer Reference/Case Study

 [ ] Partner Co-Marketing
 
 [ ] Other: 


#### Request Info:

+ Request Topic/ Title:
+ Request Due Date:
+ Meeting Location:
+ Related Issue: 
+ GitLab Point of Contact/ Person Requesting:
+ Partner Company:
+ Partner POC: 
+ Partner POC Contact Info: 


#### Please provide additional details of activity: include expectations, positioning, goals, and specific specialities needed



* Objective/ description of what you need:




#### Top three points to convey: 

1.

2.

3.


 
/label ~"sm_request" ~"PubSec Partner Marketing"  ~"Strategic Marketing" ~"sm_req::new_request" ~"pmm" ~"mktg-status::plan"
/weight 1
