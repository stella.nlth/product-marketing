This issue template outlines the tasks involved in the planning, executing and the follow up work that is needed to manage a CAB session.

Speakers are managed within the individual CAB session issue; these issues are located in the relevant [epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1509).

- [CAB Membership Sheet](https://docs.google.com/spreadsheets/d/1pLVOgWh_gum0nylurLgjMnnPOCe68KZ4EkSqCpYJ_DA/edit#gid=1435634710) (for adding new members/managing nominations and emailing follow up etc)
- [CAB Slide Deck](https://docs.google.com/presentation/d/11YI6zj3qMeTwXTmdovuZLEM2i3D-eNMdNXcQgc9coc8/edit)
- [CAB Agenda](https://docs.google.com/document/d/1oRPiyYkC-mv0O9BGMGwdj__6DDNlOXxL5h4eyvNyJ2w/edit)


**Pre Event**
- [ ] Confirm Product and Customer Speaker 2 months before the session date (update ReferenceEdge with customer details)
- [ ] Advise Product and Customer Speaker of timings and presentation expectations (send customer use case slide)
- [ ] Schedule a dry run session with the product speaker and customer speaker appx 1 week prior to go through the slides
- [ ] Confirm final slide delivery date (2 days pre the live CAB session) with the speakers
- [ ] Update the Slide deck and Agenda Deck with new content
- [ ] Review the standard content on the other slides re team members and ensure its still valid
- [ ] Add new CAB members to the deck and advise the new members they will be asked to give a brief intro at the session
- [ ] Confirm support team (slide controller/ note taker/ facilitator) prior to the session
- [ ] Schedule an operations dry run appx 2 days prior to run through the process with the support team etc. 

**Live Event**
- [ ] CAB Manager opens the CAB session 10 min before the go live time to get the deck ready and the support team in place
- [ ] CAB Manager presses "record to computer" at appx 2 mins past the hour and opens the session greeting the members 
- [ ] CAB Manager follows this [script](https://docs.google.com/document/d/1lDc4ebN1IcJjLZZ8nZUHXK9jKOjAbzrPLIWE-PAjVlY/edit) and adapts as appropriate
- [ ] CAB Manager faciliates the speaking sessions (intros the speakers /takes notes/ flags questions etc) 
- [ ] CAB Manager closes the CAB session (advises the members of the agenda for the next session and thanks speakers) 

**Post Event**
- [ ] CAB Manager uploads the recording from their PC to the [CAB drive](https://drive.google.com/drive/folders/1dTuEX5ybadL-eRXywN2fIUnEmo_lW9nD)
- [ ] CAB Manager creates the follow up email ([see prior example](https://docs.google.com/document/d/1BVwPh7u4cJwZMRJu1iim1God4xoJLT8byXB2JzSUR10/edit)) tailoring it for the session and sends it outlines
- [ ] CAB Manager uploads the recording to the CAB Slack Channel
- [ ] CAB Manager updates the [CAB Page](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/CAB/) with theme/attendance information from the session. 
- [ ] CAB Manager thanks internal GitLab speaker in "thanks" slack channel and send a TY note to the customer also. 


