**Market Research Request**

**Requestor:** 
<*Who is this for or who are you?*>

**Research Topic:** 
<*What is your topic/title that you want to study?*>

**Why:**
<*What is your business objective and underlying rationale for this research?  Is it for a campaign, product development, validate current messaging, etc?*>

**Research Hypothesis:** 
<*The hypothesis is vital and is the underlying driver for your research.  It defines the question that you want answers to.  A hypothesis needs to be specific and focused so that the research firm/analyst can design and construct a study to uncover the answer*>

**Target Respondent/Demographics:**
<*Who do you expect the survey/study to ask the questions of.  What are your target demographics by title, region, company size, etc*>

**Research Type**
<*pick one or both*>

  * [ ] Quantitative
  * [ ] Qualitative

**Budget:**
<*Do you have budget allocated and if so, how much?*>

**Timeframe:**
<*When do you want it?  Tomorrow is not an option.*>
    
**Suggested Firm/Analyst:**



**End of the template.**


/label ~"Market Research" ~"mktg-status::plan" ~"Product Marketing" ~"Market Strat and Insights"
/assign @rragozine @traci
