<!-- Please title 'Analyst Citation Review' - [(content type) draft content title] e.g. Analyst Citation Review - (Blog) How AI will change software development -->

**Requestor:** <!-- use GitLab ID please -->

**Content type:** <!-- for internal content being created e.g. blog, presentation, tweet, web page -->

**Content title:**   <!-- for internal content being created; draft title okay -->

**Analyst firm:**

**Analyst source(s):** <!-- include link(s) to report(s) or webinar(s) that include(s) the excerpt(s) you'd like to reference -->

**Requested excerpt(s):** <!-- Note: Forrester and Gartner require us to quote their research verbatim; even paraphrasing that maintains the original meaning is prohibited. -->

<!-- NOTE: If published research does not meet your requirements and a custom quote is required, we may be able to have an analyst from 451 Research, IDC, or RedMonk provide one. In this case, please replace **Requested excerpt(s):** with **Custom quote request:** and add either a draft quote or the key points you would like the analyst to convey in their quote. -->

**Date approval needed:** <!-- note Forrester has a 2-business day turnaround while Gartner can take up to 4 business days (unless we have reprint rights then Gartner MAY be able to turn around in as little oon as 1-2 days)-->

**Related links:**
- Drive folder - @rragozzine <!-- submitted PDFs, approval emails, requested changes, etc.-->
- Draft content, with excerpt and attribution included: - Requestor
  - Google Doc/Review App/Screen Capture/Other <!-- add link to applicable option-->
  - MR, if applicable <!-- delete line if not applicable-->
- Analyst firm requested changes (if applicable) - @rragozzine
- Analyst approval email - @rragozzine
- Link to final content - Requestor <!-- if an existing asset like a web page, add link now; if a yet-to-be created asset, add link once final -->

**Next Steps:**
- [ ] Add proposed quote(s)/graphic(s) in adherence with firm policy (see Key Resources below) - Requestor 
- [ ] Add link to externally shareable draft content, with excerpt and attribution included (see Related links above) - Requestor
- [ ] Request approval from analyst firm - @rragozzine
- [ ] Add link to analyst-requested changes to Related links above, and share with requestor or N/A - @rragozzine
- [ ] Confirm requested changes have been made - Requestor (or @rragozzine if N/A)
   - [ ] Resubmit for approval or N/A - @rragozzine
   - [ ] Confirm firm approval of resubmitted content or N/A - @rragozzine
- [ ] Add link to final asset to Related links above - Requestor


**Key Resources:**
- [451 Research Citation Policy](https://docs.google.com/document/d/1nnFS7pRzRmUSirCnWDJwC-nWtNe2c_IwCAarSUk_yPI/edit?usp=sharing)
- [Forrester Citation Policy](https://go.forrester.com/policies/citations-policy/)
- [Gartner Citation Policy](https://www.gartner.com/en/about/policies/content-compliance)
- [IDC Citation Policy](https://drive.google.com/file/d/1A6yXoNSINIKkRRbQASyqRS-NY2yVZIgd/view?usp=sharing)


/label ~"Analyst Relations" ~"AR-Research" ~"Strategic Marketing" ~"sm_req::assigned" ~"mrnci" ~"mktg-status::plan" ~"sm_request" ~"pmM::Internal" ~"ar-s::in-process"
/assign @rragozzine
/confidential
