<!-- Please title 'Analyst Speaker Request' - [(event type) event title or focus] e.g. Analyst Speaker Request - (Webinar) Why DevSecOps, Why Now -->

**Event Title:** <!-- or general focus, if title is TBD)-->

**Event Type:**
- [ ] Onsite (Location: )
- [ ] Virtual 

**Event Date:** <!-- or general timeframe, if date is TBD)-->

**Speaker Role:**
- [ ] Keynote 
- [ ] Track session
- [ ] Other (define)

**Session Type:**
- [ ] Fireside chat
- [ ] Panel discussion
- [ ] Q&A
- [ ] Single presenter
- [ ] Other (define)

**Additional Background:** 

**Allocated Budget:** <!-- Costs can range from $14K for a Forrester speaker with no replay rights to $73K for a pre-recorded Gartner presentation which includes replay rights -->

**Requested Analyst(s) / Firm(s):** 

**Team members involved:**
- Analyst relations:
- Co-presenter (if applicable): 
- Content lead: 
- Event/project coordination:
   - Analyst firm: 
   - GitLab: 

**Related Epic/Issue(s):**


/label ~"Analyst Relations" ~"Strategic Marketing" ~"AR-Strategy" ~"mktg-status::wip" ~"mrnci" ~"sm_request" ~"pmM::Internal" ~"sm_req::assigned"
/assign @rragozzine
/confidential
