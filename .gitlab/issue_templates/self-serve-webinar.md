
*   Name:
*   Job:
*   Website:
*   Topic:
*   Abstract:
*   URL event:
*   Date: 
*   Backlink:
*   Anchor text:
*   Giveaway:
*   Poll questions:
*   Seed questions:



---




*   Dry run date:
*   Dry run template script:
*   Dry Run Outline
*   Discuss Housekeeping:

Video on or off ?

Remind presenter to turn notifications off

Remind webcast team to arrive 30 mins early on the day of 

Check everyone’s ability to share on zoom

Confirm who will be Presenter and Moderator

Do we want a poll? Ask if we should share results with the audience or just read it out.



*   Confirm Webcast outline:

Intro & housekeeping - Time - DRI

Polling (start) - Time - DRI

Presentation  - Time - DRI

Q&A  - Time - DRI

(optional) If slides are ready Presenter can share and obtain feedback 

Get 3 canned Qs for Q&A session:

Qs 1:

Qs 2:

Qs 3:

      



*   Additional feedback/Discussion items:
*   Next steps:

Script - Single presenter with poll

Welcome/Intro/Housekeeping

Hello and welcome everyone. Thanks for joining us today, we are very excited to have you on! In this webcast, we will be covering a hot topic on how to Secure your applications in a Cloud Native world by leveraging Zero Trust principles.

My name is Agnes, and I work on the marketing programs team here at GitLab. I’m joining you from Jakarta, Indonesia today! We'd love to hear where everyone is tuning in from so please use the chat function to say hi and tell us where you are in the world!

Before we get started, I’m going to cover a couple of housekeeping items. First, feel free to ask questions throughout the presentation. You can use the Q&A function at the bottom of your screen for that. We’ll have dedicated time for questions at the end of the webcast, but you can go ahead and send in your questions as you think of them and we’ll make sure we get to them at the end.

If you are experiencing any technical difficulties, you can use the chat function to get in touch with me, the moderator for help. Lastly, we will be recording today’s presentation just so everyone in attendance is aware. The recording will be delivered to all registrants in the next few days.

Our presenter today is Cindy Blake, GitLab's senior product marketing manager specializing on Security topics.

We're going to launch a couple of polls throughout the webcast, so we learn more about you! That way, Cindy can tailor the presentation accordingly. 

With that, I am going to launch our first poll....

[Share poll]

TY for participating in the poll! Now I am going to turn the presentation over to Cindy.

----------------------

Q&A Section

1. Canned qs 1 How much of what you recommended does GitLab offer? 

2. Canned qs 2 Can you give an example of auto remediation?

3. Canned qs 3 What is the best way to build auditability into my software development lifecycle?

---------------------

End

Thank you Cindy for the informative session!

That’s all for today, thank you so much for joining us! We will be sending over the recording from this webcast in the next few days, so look out for it :) Thanks again!

Webinar checklist:

[ ] Block calendars for all hosts and panelists starting 30 minutes before the event start time.

[ ] Ensure there are 2 co-hosts for a Zoom webcast to avoid any recording disruptions (i.e should one person lose internet connection).

 
