Title: Customer case study - [Account Name]

**Check to ensure this issue is marked as confidential**

### Account Overview

+ **Account Name**:
+ **Account Owner**:
+ **Industry Vertical**: 
+ **Primary Contact/Champion's Name**: 
+ **Champion's Title**:
+ **Champion's Timezone**:
+ **Requested date-time for intro call**:

### Pre-call Resources

+ **Current GitLab products and subscription**: (e.g. Ultimate, Premium, Starter, Core, Gold, Silver, Bronze, Free etc.) 
+ **Reason they went with GitLab**: 
+ **List competitors we won this account from**: (e.g. CE, Legacy SCM, BitBucket, GitHub, Jenkins, etc.)  
+ **Latest version.gitlab ping**:
+ **Existing Public mentions of GitLab or other tools**:
+ **Suggested case study focus area**: 

#### Copy and paste the label and assign commands into a comment  

**FINAL COPY APPROVED TO BE PUBLISHED IS LINKED HERE BY CR MANGER**

