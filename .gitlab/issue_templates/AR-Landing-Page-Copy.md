<!-- Purpose of this issue: To write copy for a landing page. -->

## Action Items Checklist
* [ ] Name this issue `Landing Page Copy: <analyst content name>` (ex. Landing Page Copy: 2020 MQ for ARO)
* [ ] Add to relevant epic and Set due date based on [timeline guidelines](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029) - note these are recommendations, up for discussion in collaborating with the team.
* [ ] Provide type of Marketo landing page (select one)
   - [ ] [Gated Content](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
   - [ ] Other: 
* [ ] Copy: `add / link to new tab in googlesheet` [Character Limit Checker](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=905304679)
* [ ] Reviewers/Approvers: `@gitlab-handle of relevant individuals`

label ~"mktg-status::wip" ~"Analyst Relations"  ~"sm_req::assigned" ~"Strategic Marketing" ~"mrnci" ~"mktg-status::plan" ~"sm_request" ~"pmM::Internal"
/assign @rragozzine
/confidential

<!-- DO NOT UPDATE - PROECT MANAGEMENT
/label ~"mktg-status::wip" ~"Analyst Relations" 
-->
