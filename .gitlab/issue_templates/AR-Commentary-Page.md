**Analyst Firm:**

**Title:**

**Document List**
*  Final report(s)
*  Inputs doc
*  WIP commentary page

**DRI's**
*  Analyst Relations 
*  Competitive Intelligence
*  Product Management
*  Product Marketing
*  Technical Marketing

**To do**
  * [ ] Create / link to inputs doc 
  * [ ] Create / link to WIP commentary page (see [handbook](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#analyst-report-pages))
  * [ ] Add inputs to WIP commentary page 
  * [ ] Create / link to AR citation review issue 
  * [ ] Confirm analyst firm for approval
  * [ ] Remove WIP status

**Related issues and links**
*  Report epic
*  Drive folder


/label ~"Analyst Relations" ~"Strategic Marketing" ~"AR\-WaveMQ" ~"mktg-status::wip" ~"mrnci" ~"sm_req::assigned" ~"sm_request" ~"pmM::External" ~"ar-p::1" ~"ar-s::queue
/assign @rragozzine
/confidential
