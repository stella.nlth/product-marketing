<!-- 
Issue Title:
"\<Use Case Abreviation\> Solution Overview Sim Demo" (eg. "CI Solution Overview Sim Demo") (don't type the "\" as those are escapes for the greater-than signs). Another example of a "use case abbreviation" is VCC for the use case "Version Control and Collaboration". 
-->

### Summary
Make a [Simulation Demo (Sim Demo)](https://about.gitlab.com/handbook/marketing/product-marketing/demo/sim-demos/) out of the Solution Overview Demo.

### Acceptance Criteria
* [ ] Length: <= 10 mins
* [ ] Resolution: 1920 x 1080
* [ ] Each step should have speaker notes that explains what to do AND explains why and what it means to the audience. This is where the demo script is put.

### Production
* [ ] Capture screenshots and minor animation sequences from video (with video in full screen)
* [ ] Add click points
* [ ] Render as stand alone demo executable for both Mac and Windows
* [ ] Render as html5

### Publishing
* [ ] Publish sim demo to Learn@GitLab page at https://about.gitlab.com/learn/. Details on [asset inventory page](https://about.gitlab.com/handbook/marketing/product-marketing/asset_inventory/)
* [ ] Add the final asset to the [SM Inventory Data spreadsheet](https://docs.google.com/spreadsheets/d/1W5oAlbPV610-ylM7LWv_zc6bEqQK9dI0H9Hrn2f6Jwc/edit#gid=0)
* [ ] Add sim demo to [GDrive Simulation (demo) Folder](https://drive.google.com/drive/u/0/folders/1q0yk09WojAu9ivDHTVQEKQ8xBACFbTTM) in a folder named after the demo. Make sure to store the rendered files AND the source so others can contribute)
* [ ] Announce new video (with link) to Slack channels: #marketing

<!-- Don't remove anything below this line -->
/label ~"Strategic Marketing" ~"mktg-status::plan" ~"sm_request" ~"sm_req::backlog" 
/label ~"usecase-gtm" ~"tech-marketing" ~"tech-marketing::demo"
/weight 1

<!-- Add from below this line as necessary: 
/label ~"\<label for the use case\>"
/milestone %\<milestone\> 
/epic &\<XXXXXX\> 
/assign @\<assignee\>
   + /label ~"sm_req::assigned"
/due \<due date\>
--\>



