# **Title: Migrate Comparison Request**  

<!-- 
Issue Title:
"<Comparison Name> Comparison Migration" (eg. "CircleCI Comparison Migration")
-->

### Summary
Migrate the comparison page for the competitor in the title to the new format so that it is more attractive and maintains interest and eyeballs for longer. 

This migration will consist of a new page template, a new category comparison infographic (which needs to be created), and moving all remaining data into the new template as is.

### Files
- [Analysis Spreadsheet](https://docs.google.com/spreadsheets/d/1Dd1wraHGVM21L942PsYxNj8Czfvq9XO7ysG1Gge5YEk/edit?ts=5f4996cc#gid=90532820)
- [New Comparison Page - How To Build and Edit](https://docs.google.com/presentation/d/1C8ESioVnJES0_hdHn_ThtpGID_0tPacZXWgT6srVsGA/edit#slide=id.g92f19fb15b_0_36)

### Steps
1. Validate competitor scores
   - [ ] Review the scores in the `Analysis Spreadsheet` on the 'Comparison DATA ENTRY Sheet' tab (derived from `features.yml`)
   - [ ] Review the competitor website (to verify the data in the spreadsheet is current). Does the Competitor offer Category XYZ (look across the stages)?
   - [ ] Update the spreadsheet on the 'Comparison DATA ENTRY Sheet' with a 1 or a 0 to reflect your assessment (1 = competitor has something in category, 0 = competitor does not)
1. Create the infographic
   - [ ] Use Figma template identified in `New Comparison Page - How To Build and Edit` + the 'FIGMA_DATA' tab from the `Analysis Spreadsheet`
   - [ ] Follow the instructions in the `New Comparison Page - How To Build and Edit` under 'Stage 1' section.
1. Create new comparison page using new template (**please wait to do these last steps until 2020-09-04** as we still have a few edits to complete to the features_v2.yml file to prepare it)
   - [ ] Follow instructions in `New Comparison Page - How To Build and Edit` on updating `features_v2.yml` (includes new infographic)
   - [ ] Migrate extra data from current comparison page to new page
   - [ ] Assign MR to Mahesh to before publishing with new infographic
   - [ ] Once published, close this issue and be sure to check off the comparison in the parent Epic

## SM section
#### [Refinement questions](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/sm-project-management/#workflow---refine-sprint-retrospective)


When reviewing this issue/ request consider the following:

- [ ] **DRI**: Has a DRI been identified and do they accept responsibility? 
- [ ] **Clear**: Are the expectations and work required unambiguous and clearly defined?
- [ ] **Completable**: Is there a clearly-stated definition of done?
- [ ] **Unconstrained**: Is the work free of blockers and dependencies? *If not, address blockers first or leave in Product Backlog until blockers are removed.*
- [ ] **Achievable**: Is the issue something that can be completed within a 2-week timebox? *If not, decompose it.*
  

/label ~"sm_request" ~"Strategic Marketing" ~"sm_req::backlog" ~"mktg-status::plan" ~"refine::unrefined"  
/label ~pMm ~"tech-marketing"  
/label ~"pmm::Web" ~"tech-marketing::web"  
  
/weight 1  
/epic &242   
/milestone %"uc-backlog"  
