# **Release highlights**  

- [ ] Add subject "Release Post Highlights: xx.xx"
- [ ] Assign to PMM lead for the release
- [ ] Review the release post MR for features being released (see pinned in #release-post slack channel)
- [ ] Create 3-4 themes for the release post with few lines in description
- [ ] Tag PR team (e.g., Christina Weaver) and Field Enablement team (e.g., Shannon Thompson)

[Example release post highlights](https://docs.google.com/document/d/1e8Am6HyLNLyxI7xPbWg6sVS-6pMTZtqlj0Aze4smZwg/edit) 

## Release theme 1

Description 

## Release theme 2

Description 

## Release theme 3

Description 

## Release theme 4

Description 



/weight 1
