**Template is to assign duties when a customer case study is approved by the customer.
Activity will be driven by regional CRM, Fiona to lead EMEA/APAC and TBA the Americas
Please delete if you are not the assignee or the task is not appropriate for that customer**


**Internal**


 - [ ] Case Study is approved and logo form uploaded to SFDC account @FionaOKeeffe

 - [ ] PR opportunity offered to the customer (If accepted, PR issue is created and linked to case study issue) @FionaOKeeffe 

 - [ ] PR plan and outputs are loaded into the PR issue @FionaOKeeffe 

 - [ ] Case study is posted to our website and uploaded on the grid @FionaOKeeffe 

 - [ ] Case study loaded into Path Factory track for campaigns and ABM  @FionaOKeeffe

 - [ ] Case study is loaded into Reference Edge and reference details updated @FionaOKeeffe 

 - [ ] Case study posted on internal content slack channel @FionaOKeeffe

 - [ ] Case study posted on sales/regional slack channels @FionaOKeeffe 

 - [ ] Case study tagged to Sales/Comms Manager for inclusion in newsletter and for the teams to post to their networks (LI Link) @FionaOKeeffe 

 - [ ] Customer is sent the case study website link plus links to Social Media and PR activities to share with their networks @FionaOKeeffe 

 - [ ] Customer SWAG gift issued @FionaOKeeffe 

 - [ ] High-Quality logo uploaded to [Gdrive](https://drive.google.com/drive/folders/1L_CUjvW_1cOmqjLCPUsAqqr7D29jW-Vb) in requisite bespoke customer folder by @FionaOKeeffe 

 - [ ] Update the Sales Decks [create a slide per case study](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/3465) @FionaOKeeffe 

 - [ ] Add approved logo to logo decks and [tracking sheet](https://docs.google.com/spreadsheets/d/1wnczZQ7a8rIXI_OCNPuqJv5VeC5gGs-lwAmlgyxUlvU/edit#gid=0) @FionaOKeeffe 

 - [ ] Create the localisation issue for the case study - [EMEA/APAC](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/2678) @FionaOKeeffe 

 - [ ] Add the published case study to the [case study board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=✓&state=opened) @FionaOKeeffe 

 - [ ] Update the [Metrics Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/987) @FionaOKeeffe 

 - [ ] Update the case study to the [Marketing Calendar](https://docs.google.com/spreadsheets/d/1c2V3Aj1l_UT5hEb54nczzinGUxtxswZBhZV8r9eErqM/edit#gid=571560493) @FionaOKeeffe 

 - [ ] Add standout quotes from case study to quote management board @FionaOKeeffe 

 - [ ] Update given in marketing call  @FionaOKeeffe 

 - [ ] Add to [Highspot](https://gitlab.highspot.com/).([process here](https://about.gitlab.com/handbook/sales/field-communications/gitlab-highspot/#embedding-links) to embed links). Please ensure to categorise correctly, where a cover image does not migrate over, please use the customer logo as the default image. @FionaOKeeffe  

**External**

 - [ ] Add to [Bambu](https://app.getbambu.com/feed) @FionaOKeeffe

 - [ ] Case study is added to G2 

 - [ ] Case study included in customer newsletter @FionaOKeeffe

 - [ ] Signature for Sales - Sigstr  @FionaOKeeffe 

 - [ ] Promote to GitLab Heroes @FionaOKeeffe 

 - [ ] Connect to the customer champions on LinkedIn @FionaOKeeffe 


