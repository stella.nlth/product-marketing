**Requestor:** <!-- use GitLab ID please -->

**Research Topic:**

**Requested Attendees (if applicable):**
- Required: 
- Optional: 

**Description:**

**Related links:**

**Requested Firm/Analyst:**

**Date Needed:**

**Inquiry Issue(s):**


/label ~"Analyst Relations" ~"AR-Research" ~"Strategic Marketing" ~"sm_req::new_request" ~"mrnci" ~"mktg-status::plan" ~"sm_request" ~"pmM::Internal"
/assign @rragozzine
/confidential
